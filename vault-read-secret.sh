#!/bin/bash

ARG=$1

export VAULT_ADDR="https://vault.az.oulman.dev:8200"
export VAULT_RESPONSE=$(vault write auth/azure/login -format=json role="demo-vm-reader" \
     jwt="$(curl -s 'http://169.254.169.254/metadata/identity/oauth2/token?api-version=2018-02-01&resource=https%3A%2F%2Fvault.az.oulman.dev' -H Metadata:true | jq -r '.access_token')" \
     subscription_id=$(curl -s -H Metadata:true "http://169.254.169.254/metadata/instance?api-version=2017-08-01" | jq -r '.compute | .subscriptionId')  \
     resource_group_name=$(curl -s -H Metadata:true "http://169.254.169.254/metadata/instance?api-version=2017-08-01" | jq -r '.compute | .resourceGroupName') \
     vm_name=$(curl -s -H Metadata:true "http://169.254.169.254/metadata/instance?api-version=2017-08-01" | jq -r '.compute | .name') )
export VAULT_TOKEN=$(echo ${VAULT_RESPONSE} | jq -r '.auth.client_token')

if [[ ${ARG} == "print-token" ]]; then
    echo ${VAULT_RESPONSE} | jq '.auth.client_token = "SANITIZED" | .auth.accessor = "SANITIZED"'
    echo
fi

vault kv get -format=json secrets/vault-demo | jq -r '.data.data'
